from rest_framework import viewsets
from .serializers import *
from .models import *


class TeacherView(viewsets.ModelViewSet):
    serializer_class = TeacherSerializer
    queryset = Teacher.objects.all()
    

class ClassesView(viewsets.ModelViewSet):
    serializer_class = ClassesSerializer
    queryset = StudentClass.objects.all()

    def perform_create(self, serializer):

        teacher_id = self.request.data.get("class_teacher")
        class_teacher = Teacher.objects.get(pk=teacher_id)
        serializer.save(class_teacher=class_teacher)

class StudentView(viewsets.ModelViewSet):
    serializer_class = StudentSerializer
    
    def get_queryset(self):
        class_id = self.request.GET.get("class", None)
        if class_id:
            return Student.objects.filter(student_class_id=class_id)

        return Student.objects.all()

    def perform_create(self, serializer):

        surname = self.request.data.get("surname")
        firstname = self.request.data.get("firstname")
        student_number = self.request.data.get("student_number")
        class_id = self.request.data.get("student_class")

        student_class = StudentClass.objects.get(pk=class_id)
        student = Student(student_number=student_number, firstname=firstname, surname=surname, student_class=student_class)
        student.save()