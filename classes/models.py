from django.db import models
from django.utils import timezone

class Teacher(models.Model):
    firstname = models.CharField(max_length=255)
    surname = models.CharField(max_length=255)
    id_number = models.CharField(max_length=50)
    date_added = models.DateTimeField(default=timezone.now)

    class Meta:
        db_table = 'public"."teacher'

    def __str__(self):
        return self.firstname + ' ' + self.surname
    
class StudentClass(models.Model):
    class_label = models.CharField(max_length=255)
    class_teacher = models.ForeignKey(Teacher, on_delete=models.CASCADE, related_name="classes")
    date_added = models.DateTimeField(default=timezone.now)

    class Meta:
        db_table = 'public"."classes'

    def __str__(self):
        return self.class_label

class Student(models.Model):
    firstname = models.CharField(max_length=255)
    surname = models.CharField(max_length=255)
    student_number = models.CharField(max_length=50)
    date_added = models.DateTimeField(default=timezone.now)
    student_class = models.ForeignKey(StudentClass, on_delete=models.CASCADE, related_name="class_student")

    class Meta:
        db_table = 'public"."student'

    def __str__(self):
        return self.student_number
