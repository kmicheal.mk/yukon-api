from rest_framework import serializers
from .models import *


class TeacherSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)

    class Meta:
        model = Teacher
        fields = "__all__"

class ClassesSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    teacher = serializers.PrimaryKeyRelatedField(read_only=True, required=False)
    teacher_details = serializers.SerializerMethodField(read_only=True)

    def get_teacher_details(self, class_object):
        return TeacherSerializer(class_object.class_teacher).data
    
    class Meta:
        model = StudentClass
        fields = "__all__"

class StudentSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    student_class = serializers.PrimaryKeyRelatedField(read_only=True, required=False)
    class_details = serializers.SerializerMethodField(read_only=True)

    def get_class_details(self, student):
        return ClassesSerializer(student.student_class).data
    
    class Meta:
        model = Student
        fields = "__all__"