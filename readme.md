## Installation

Create virtual env

`python -m venv yk`

Install requirements

`pip install -r requirements.txt`

Create postgress DB, add it's credenitals in a `.env` file, see sample from `.env.sample`

Run migrations and start the app.
`python manage.py makemigrations`

`python manage.py migrate`

`python manage.py runserver`