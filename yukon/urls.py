from django.urls import include, path
from rest_framework import routers
from classes import views as classes_views
from django.contrib import admin
from django.urls import path

router = routers.DefaultRouter()
router.register(r'classes', classes_views.ClassesView, basename='classes')
router.register(r'teachers', classes_views.TeacherView, basename='teachers')
router.register(r'students', classes_views.StudentView, basename='students')


urlpatterns = [
    path('api/', include(router.urls)),
    path("admin/", admin.site.urls),
]
